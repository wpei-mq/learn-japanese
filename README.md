## Learn Japanese in a Flash

### Proposed functionality:
1. large cell with kanji to be identified within a time limit
2. 4 smaller cells underneath with multiple choice answers
3. if correct answer is clicked, move onto next word and add on additional time 
equal to previously added time - 1 second
4. start button, difficulty button (easy, medium, hard)
5. stop button, clears screen, resets clock
6. pause button, clears screen, pauses clock

Refer to: http://www.saiga-jp.com/language/kanji_list.html for kanji list