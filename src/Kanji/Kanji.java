package Kanji;

import java.util.Random;

public class Kanji {

    private static String[] kanjiGrade1 = {"一","右","雨","円","王","音","下","火","花","貝",
            "学","気","九","休","玉","金","空","月","犬","見","五","口","校","左","三","山",
            "子","四","糸","字","耳","七","車","手","十","出","女","小","上","森","人","水","正","生","青","夕",
            "石","赤","千","川","先","早","草","足","村","大","男","竹","中","虫","町","天","田","土","二","日",
            "入","年","白","八","百","文","木","本","名","目","立","力","林","六"};

    private static String[] grade1Pron = {"Ichi","Migi","Ame","En","Ō","Oto","Shita",
            "Hi","Hana","Kai","Gaku","Ki","Kyū","Kyū",
            "Tama","Kin","Sora","Tsuki","Inu","Mi","Go","Kuchi","Kō","Hidari","San",
            "Yama","Ko","Shi","Ito","Ji","Mimi","Nana","Kuruma","Te","Jū","Shutsu","On'na","Ko","Jō",
            "Mori","Hito","Mizu","Masa","Nama","Ao","Yū","Ishi","Aka","Sen","Kawa","Saki",
            "Haya","Kusa","Ashi","Mura","Ō","Otoko","Take","Naka","Mushi","Machi","Ten","Ta",
            "Tsuchi","Ni","Hi","Iri","Toshi","Shiro","Hachi","Hyaku","Bun","Ki",
            "Hon","Na","Me","Tachi","Chikara","Hayashi","Roku"};

    private static String[] grade1Def = {"One","Right","Rain","Circle/Yen/Round/Money","King","Sound/Noise/Note","Under/Below",
            "Fire/Flame/Light/Blaze","Flower/Blossom","Shellfish/Shell","Learning","Mood/Spirit","Nine","Holiday/Rest/Refreshment/Recreation",
            "Ball/Coin/Jewel/Gem","Money","Sky","Month","Dog","Look/Watch/See","Five","mouth","School","Left","Three",
            "Mountain","Child","Four","Yarn","Character","Ear","Seven","Car","Hand","Ten","Out","Woman","Small","Up",
            "Woods/Forest","Person","Water","Positive","Living","Blue","Evening","Stone","Red","Thousand","River","Ahead",
            "Early","Grass","Leg","Village","Big","Man","Bamboo","During/Within","Insect","Town","Sky/Heaven","Rice Field",
            "Earth/Soil","Two","Day/Sun","On","Year/Age","White","Eight","Hundred","Sentence/Letter/Writings","Wood/Tree",
            "Book/This/Volume","Name","Eye","Stand","Power/Strength/Ability","Small Forest","Six"};

    private String[] kanji;
    private Random r = new Random();

    public Kanji(int grade) {
        if (grade == 1) {
            int randomInt = r.nextInt(kanjiGrade1.length);
            this.kanji = getRandomKanjiGrade1(randomInt);
        }
    }

    private String[] getRandomKanjiGrade1(int r) {
        String[] strings = new String[] {kanjiGrade1[r],grade1Pron[r],grade1Def[r]};
        return strings;
    }

    public String getKanji(int i) {
        return kanji[i];
    }
}
