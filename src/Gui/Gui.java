package Gui;

import java.awt.*;
import java.time.Duration;
import java.time.Instant;

import Kanji.*;

public class Gui {

    private Cell[] cells = new Cell[3];
    private Cell[] buttons = new Cell[3];

    private int x, y, width,height;
    private Instant time;

    private Kanji kanji;

    public Gui(int x, int y) {
        this.x = x;
        this.y = y;
        width = 800;
        height = 600;

        setCells();
    }

    private void setCells() {
        // TODO: generate random kanji for the main rectangle (http://www.saiga-jp.com/language/kanji_list.html)
        kanji = new Kanji(1);
        cells[0] = new Cell(x, y, width - 49, height - 250, false, kanji.getKanji(0));

        // TODO: generate a matching pronunciation for the main triangle and 3 random ones
        for (int i = 1; i < cells.length; i ++) {
            cells[i] = new Cell(x + (i - 1) * ((width - 50) / 2 + 1), y + height - 249, (width - 50) / 2, 100, true, kanji.getKanji(i));
        }
    }

    public Cell[] getCells() {
        return cells;
    }

    public void update() {
        if (time.plus(Duration.ofSeconds(5)).isBefore(Instant.now())) {
            time = Instant.now();
            kanji = new Kanji(1);
            cells[0] = new Cell(x, y, width - 49, height - 250, false, kanji.getKanji(0));

            for (int i = 1; i < cells.length; i ++) {
                cells[i] = new Cell(x + (i - 1) * ((width - 50) / 2 + 1), y + height - 249, (width - 50) / 2, 100, true, kanji.getKanji(i));
            }
        }
    }

    public void paint(Graphics g, Point mousePosition) {
        if (time == null) {
            time = Instant.now();
        }
        for(int i = 0; i < cells.length; i++) {
            Cell thisCell = cells[i];
            thisCell.paint(g, thisCell.contains(mousePosition));
        }
        update();
    }
}
