package Gui;

import java.awt.*;
import Kanji.*;

public class Cell extends Rectangle {

    private Boolean clickable;
    private String kanji;

    public Cell(int x, int y, int width, int height, Boolean clickable, String kanji) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.clickable = clickable;
        this.kanji = kanji;
    }

    public void paint(Graphics g, Boolean highlighted) {

        g.setColor(Color.LIGHT_GRAY);
        g.fillRect(x, y, width, height);

        // highlight the cells by adding a black border
        if (clickable && highlighted) {
            g.setColor(Color.BLACK);
            g.drawRect(x, y, width, height);

            g.setColor(Color.BLACK);
            String f = g.getFont().toString();
            g.setFont(new Font(f, Font.BOLD, 15));
            g.drawString(kanji, x + width / 2 - 15, y + height / 2 + 15);
        }

        if (!clickable) {
            g.setColor(Color.BLACK);
            String f = g.getFont().toString();
            g.setFont(new Font(f, Font.BOLD, 200));
            g.drawString(kanji, x + width / 2 - 100, y + height / 2 + 80);
        }

        if (clickable && !highlighted) {
            g.setColor(Color.BLACK);
            String f = g.getFont().toString();
            g.setFont(new Font(f, Font.BOLD, 15));
            g.drawString("Mouse Over to View", x + width / 2 - 60, y + height / 2 + 15);
        }
    }

    public boolean isClickable() {
        return clickable;
    }

    @Override
    public boolean contains(Point target){
        if (target == null)
            return false;
        return super.contains(target);
    }
}

//